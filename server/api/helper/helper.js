console.log("starting helper.js")
var CryptoJS = require("crypto-js");
const encryption_key = global.application_environment.encryption_key;

function verifyToken(req, res, next) {
  // get auth header  value

  const bearerHeader = req.headers['cookie'];
  // Check if bearer is undefined
  if (typeof bearerHeader != 'undefined') {
    // split at the space
    const bearer = bearerHeader.split('uToken=');
    // Get token from array
    const bearerToken = bearer[1];
    //set the token
    req.token = bearerToken;
    // Next middleware
    next();

  } else {
    // Forbidden
    // res.sendStatus(403);
    res.json({
      status: "failed",
      // authData
    })
  }
  
}

function encryptString (messageToencrypt) {
  var encryptedText = CryptoJS.AES.encrypt(messageToencrypt,encryption_key).toString();
  return encryptedText;
}

function decryptString (string) {
  console.log("string ", string)
  var bytes  = CryptoJS.AES.decrypt(string, encryption_key);
  console.log("bytes ", bytes)
  var plaintext = bytes.toString(CryptoJS.enc.Utf8);
  console.log("plaintext ", plaintext)
  return plaintext;
}


module.exports.encryptString = encryptString;
module.exports.decryptString = decryptString;
module.exports.verifyToken = verifyToken;