module.exports = function (environment) {
	console.log("starting login.js")
	const server = require("../../server.js");
	const app = server.app;
	var url = require('url');
	const dbconnect = require("../../db.js");
	const decryptString = require("../helper/helper").decryptString;
	const verifyToken = require("../helper/helper").verifyToken;
	const sequelize = dbconnect.sequelize;
	const jwt = require('jsonwebtoken');
	const bcrypt = require('bcrypt');
    const saltRounds = 10;
    const encryption_key = environment.encryption_key;



	app.get('/api/login', (req, res) => {
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
		var email = query.email;
		var password = query.password;
        // console.log("password ", password)


		if (email && password) {
			email = query.email.toLowerCase();
        //     var decryptedPassword = decryptString(query.password);
        // console.log("encryptedPassword ", decryptedPassword)
            
			sequelize.query(`
            SELECT *
            FROM user
            WHERE email = "${email}";

            `).spread(function (userData, metadata) {
                console.log(userData)
                if (userData && userData[0]) {
                    bcrypt.compare(password, userData[0].password, function (err, result) {
                        if (result) {
                            let user = {
                                id: userData[0].id,
                                name: `${userData[0].first_name} ${userData[0].last_name}`,
                                email: userData[0].email,
                            }
                            jwt.sign({ user }, encryption_key , (err, token) => {
                                if (err) {
                                  res.status(200).send({
                                    message: "Problem in creating token",
                                    status: "invalid",
                                  });
                                } else {
                                    res.json({
                                        user,
                                        uToken: token,
                                        message: "success"
                                        // authData
                                    });
                                }
                            })
                        } else {
                            res.json({
                                status: "failed",
                                message: "invalid password"
                
                            });
                        }
                    })
                } else {
                    res.json({
                        status: "failed",
                        message: "invalid email"
        
                    });
                }
				
			});
		} else {
			res.json({
				status: "failed",
				message: "insufficient data"

			});
		}

    })
    

    app.post('/api/verifyToken', verifyToken, (req, res) => {
      
        jwt.verify(req.token, encryption_key, (err, authData) => {
          if (err) {
            res.json({
              status: "failed",
            })
          } else {
            res.json({
                status: "success",
                authData,
              })
            
          }
        });
      

      });
}

