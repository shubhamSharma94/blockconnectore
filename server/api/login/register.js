module.exports = function (environment) {
	console.log("starting register.js")
	const server = require("../../server.js");
	const app = server.app;
	var url = require('url');
	const dbconnect = require("../../db.js");
	const encryptString = require("../helper/helper").encryptString;
	const sequelize = dbconnect.sequelize;
	const jwt = require('jsonwebtoken');
	const bcrypt = require('bcrypt');
	const saltRounds = 10;



	app.post('/api/register', (req, res) => {
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
		var email = query.email;
		var firstName = query.firstName;
		var lastName = query.lastName;
		var password = query.password;
		


		if (email && firstName && lastName && password) {
			email = query.email.toLowerCase();
			firstName = query.firstName.toLowerCase();
			lastName = query.lastName.toLowerCase();
			bcrypt.hash(password, saltRounds, function(err, hash) {
				sequelize.query(`
        INSERT INTO user (first_name, last_name, email, password)
        VALUES ("${firstName}", "${lastName}", "${email}", "${hash}" ); 
            `).spread(function (user, metadata) {
				console.log(user)
				res.json({
					user,
					status: "success",
					// authData
				});
			});
			});
			
		} else {
			res.json({
				status: "failed",
				message: "insufficient data"

			});
		}

	})
}
