module.exports = function (environment) {
  console.log("starting place.js")
  const server = require("../../server.js");
  const app = server.app;
  var url = require('url');
  const dbconnect = require("../../db.js");
  const encryptString = require("../helper/helper").encryptString;
  const sequelize = dbconnect.sequelize;
  const jwt = require('jsonwebtoken');

  const encryption_key = environment.encryption_key;

  app.post("/api/place", (req, res) => {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var token = query.token;
    var name = query.name;
    var description = query.description;
    var address = query.address;
    var imageUrl = query.imageUrl ? query.imageUrl : "";

    jwt.verify(token, encryption_key, (err, authData) => {
      if (err) {
        console.log(err);
        res.json({
          status: "failed"
          // authData
        });
      } else {
        if (name && description && address) {
          sequelize.query(`
        INSERT INTO places (name, description, address, imageUrl, createdBy)
        VALUES ("${name}", "${description}", "${address}", "${imageUrl}", "${authData.user.id}" ); 
            `).spread(function (place, metadata) {
            sequelize.query(`
              SELECT *
              FROM places
              WHERE id = ${place}
            `).spread(function (place, metadata) {
              res.json({
                place
              });
            })
            
          })
        } else {
          res.json({
            success: "failed",
            message: "insufficient data"
            // authData
          });
        }
      }
    })

  })


  app.get("/api/place", (req, res) => {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var token = query.token;
    var placeId = query.placeId;

    jwt.verify(token, encryption_key, (err, authData) => {
      if (err) {
        console.log(err);
        res.json({
          status: "failed"
          // authData
        });
      } else {
        if (placeId) {
          sequelize.query(`
            SELECT * 
            FROM places
            WHERE createdBy = ${authData.user.id} 
                  AND id = ${placeId}
              `).spread(function (place, metadata) {
              console.log(place)
              res.json({
                place
                // authData
              });
            })
        } else {
          sequelize.query(`
            SELECT * 
            FROM places
            WHERE createdBy = ${authData.user.id}
              `).spread(function (place, metadata) {
              console.log(place)
              res.json({
                place
                // authData
              });
            })
        }
        
      }
    })
  })


  app.put("/api/place", (req, res) => {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var token = query.token;
    var placeId = query.placeId;
    var name = query.name;
    var description = query.description;
    var address = query.address;
    var imageUrl = query.imageUrl ? query.imageUrl : "";

    jwt.verify(token, encryption_key, (err, authData) => {
      if (err) {
        console.log(err);
        res.json({
          status: "failed"
          // authData
        });
      } else {
        if (placeId) {
          sequelize.query(`
            UPDATE places
            SET name="${name}", description="${description}", address="${address}", imageUrl="${imageUrl}"
            WHERE id = ${placeId}
          `).spread(function (place, metadata) {
              console.log(place)
              res.json({
                status: "success"
              });
            })
        } else {
          res.json({
            status: "failed"
          });
        }
        
      }
    })
  })



  app.delete("/api/place", (req, res) => {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var token = query.token;
    var placeId = query.placeId;

    jwt.verify(token, encryption_key, (err, authData) => {
      if (err) {
        console.log(err);
        res.json({
          status: "failed"
        });
      } else {
        if (placeId) {
          sequelize.query(`
            DELETE FROM places
            WHERE id = ${placeId}
          `).spread(function (place, metadata) {
              res.json({
                // place,
                status: "success"
              });
            })
        } else {
          res.json({
            status: "failed"
          });
        }
        
      }
    })
  })
}













