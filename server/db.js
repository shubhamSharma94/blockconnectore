// const Sequelize = require('sequelize-views-support');
const Sequelize = require('sequelize');
// console.log("global ", global.application_environment);
let environment = global.application_environment;
// // Local
// // const sequelize = new Sequelize('demo', 'root', 'Shubham@25', {
  const sequelize = new Sequelize(environment.db, environment.db_username, environment.db_password, {
  host: environment.host,
  dialect: 'mysql',
  omitNull: true,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  

  // SQLite only
  // storage: 'path/to/database.sqlite',

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});



// //Production
// const sequelize = new Sequelize('hrmaitrg_demo', 'hrmaitrg', 'ur7me[6KH&}y', {
//   host: 'localhost',
//   dialect: 'mysql',

//   pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000
//   },
  

//   // SQLite only
//   // storage: 'path/to/database.sqlite',

//   // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
//   operatorsAliases: false
// });

// sequelize.sync({ logging: console.log })

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


  module.exports.sequelize = sequelize;