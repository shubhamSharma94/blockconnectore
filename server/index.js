const _ = require('lodash')
const yargs = require('yargs');
const argv = yargs.argv;
console.log(argv.env)

let environment = {};
var config=require('./config.json')[process.env.NODE_ENV || 'dev'];

global.application_environment = config;
environment = config;


const server = require("./server.js");
const app = server.app;
const register = require("./api/login/register")(environment);
const login = require("./api/login/login")(environment);
const place = require("./api/place/place")(environment);
const imageUpload = require("./api/upload/image")(environment);


// const emailWithTemplates = require("./api/email/emailWithTemplates.js"); 


module.exports.environment = environment;  

// export NODE_ENV=dev 