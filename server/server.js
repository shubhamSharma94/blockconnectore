const express = require('express');
const app = express();
// const port = process.env.port || 3000;
const environment = global.application_environment;
console.log(environment)
const port = environment.server_port;
const path = require('path');
const dbconnect = require("./db.js");
const multer = require('multer');
const cors  = require('cors');
const fs  = require('fs');
const Loki  = require('Lokijs');



var bodyParser = require('body-parser');
var multiparty = require('multiparty');

var form = new multiparty.Form();
var parseFormdata = require('parse-formdata'); 



app.use(function (req, res, next) {
  // Website you wish to allow to connect

  res.setHeader('Access-Control-Allow-Origin', environment.server);
  // res.setHeader('Access-Control-Allow-Origin', 'http://192.168.1.4:3000');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  // res.setHeader('Access-Control-Allow-Headers', '*');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

// export NODE_ENV=dev 


app.use(express.static(path.resolve(__dirname, '../build')))
// app.use('/change-pass-forgot/:token', express.static(path.resolve(__dirname, '../web')))




app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies
// app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
app.use(cors());

app.listen(port, () => console.log(`Example app listening on port ${port}!`))




module.exports.app = app;