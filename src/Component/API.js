// import apiClient from "./API.js";
// this.client = apiClient(getCookie('uToken'));


export default function apiClient(token) {
    return {
  
      verifyToken: function() {
        let url =  'api/verifyToken';
        var paramObj = {
          token: token
  
        };

  
        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'POST',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },
  
    };
  }
  

  function getAsUriParameters(data) {
     var url = '';
     for (var prop in data) {
        url += encodeURIComponent(prop) + '=' + 
            encodeURIComponent(data[prop]) + '&';
     }
     return url.substring(0, url.length - 1)
  }
  
