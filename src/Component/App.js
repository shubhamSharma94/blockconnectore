import React from "react"
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Topbar from "./Topbar/Topbar";
import LoginContainer from "./Login/LoginContainer";
import PlaceListContainer from "./Place/PlaceListContainer";
import "../css/main.css";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeLoginInfo } from '../redux/actions/changeLoginInfo';
import { updateUserInfo } from '../redux/actions/updateUserInfo';

import { ToastContainer } from "react-toastify";

import apiClient from "./API";
import { getCookie } from "./utils";

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		}
		this.client = apiClient(getCookie('uToken'));
	}

	componentWillMount() {
		this.client.verifyToken().then((json) => {
			if (json.status === "success") {

				let userData = json.authData.user;
				userData.token = getCookie('uToken');
				this.props.updateUserInfo(userData)
				this.props.changeLoginInfo(true)
			} else {
				this.props.changeLoginInfo(false)
			}
		})
	}


	render() {
		return (
			<Router>

				{
					this.props.isLoggedIn ? (
						<div>
							<Topbar />
							<div className="slate">
								<Route exact path="/" component={PlaceListContainer} {...this.state} />
								{/* <Route component={PlaceListContainer}/> */}
							</div>
						</div>

					) : (
							<LoginContainer {...this.state} updateUserData={this.updateUserData} />
						)
				}
				<ToastContainer />
			</Router>
		)
	}
}



const mapStateToProps = state => {
	return {
		isLoggedIn: state.changeLoginInfo.isLoggedIn,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changeLoginInfo: bindActionCreators(changeLoginInfo, dispatch),
		updateUserInfo: bindActionCreators(updateUserInfo, dispatch),


	};
};



const connector = connect(mapStateToProps, mapDispatchToProps);


export default connector(App);
