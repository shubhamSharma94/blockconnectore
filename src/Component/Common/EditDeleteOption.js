import React from "react";

function EditDeleteOption (props) {
    return (
        <div 
            className="edit-delete-option enable"
            onClick={(e) => {e.stopPropagation();}}
        >
            <span 
                className="glyphicon glyphicon-pencil"
                onClick={() => {props.handleEditClick(props.itemId)}}
            ></span>
            <span 
                className="glyphicon glyphicon-trash"
                onClick={() => {props.handleDeleteClick(props.itemId)}}
            ></span>
        </div>
    )
}

export default EditDeleteOption; 