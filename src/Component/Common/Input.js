import React from "react";

export function Input (props) {
    return (
        <input 
            type={props.type ? props.type : "text"}
            className={`form-control ${props.className ? props.className : ""}`}
            placeholder={props.placeholder ? props.placeholder : "Please Input"}
            value={props.value ? props.value : ""}
            onChange={(e) => {
                props.handleChange(e.target.value, props.inputKey)
            }}
            
        />
    )
}


export function TextArea (props) {
    return (
        <textarea 
            className={`form-control ${props.className ? props.className : ""}`}
            placeholder={props.placeholder ? props.placeholder : "Please Input"}
            value={props.value ? props.value : ""}
            onChange={(e) => {
                props.handleChange(e.target.value, props.inputKey)
            }}
            
        />
    )
}