
export default function apiClient(token) {
    return {
  
      login: function(email, password) {
        let url = 'api/login';
         debugger
        var paramObj = {
          email,
          password
        };

        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'GET',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },

      register: function(firstName, lastName, email, password) {
        let url = 'api/register';
         debugger
        var paramObj = {
          token: token,
          firstName,
          lastName,
          email,
          password
        };

        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'POST',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },

    };
  }
  

  
  function getAsUriParameters(data) {
     var url = '';
     for (var prop in data) {
        url += encodeURIComponent(prop) + '=' + 
            encodeURIComponent(data[prop]) + '&';
     }
     return url.substring(0, url.length - 1)
  }
