import React from "react";
import LoginPage from "./LoginPage.js";
import apiClient from "./API";
import { getCookie } from "../utils";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateUserInfo } from '../../redux/actions/updateUserInfo';
import { changeLoginInfo } from '../../redux/actions/changeLoginInfo';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const bgImage = "https://images.unsplash.com/photo-1499678329028-101435549a4e?ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80";

class LoginContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			showLoginForm: true,
			loginEmail: "",
			loginPassword: "",

			registerEmail: "",
			registerFirstName: "",
			registerLastName: "",
			registerPassword: "",
			registerConfirmPassword: "",
			
		}
		this.client = apiClient(getCookie('uToken'));
		this.toggleLoginForm = this.toggleLoginForm.bind(this);
		this.handleChangeInput = this.handleChangeInput.bind(this);
		this.handleLoginButtonClicked = this.handleLoginButtonClicked.bind(this);
		this.handleRegisterButtonClicked = this.handleRegisterButtonClicked.bind(this);
		

	}

	toggleLoginForm() {
		this.setState({
			showLoginForm: !this.state.showLoginForm,
		})
	}

	handleChangeInput (value, type) {
		this.setState ({
			[type]: value,
		})
	}

	handleLoginButtonClicked () {
		let email = this.state.loginEmail;
		let password = this.state.loginPassword;
		this.client.login(email, password).then((json) => {
			if (json.user) {
				let userData = json.user;
				let token = json.uToken;
				userData.token = token;
				this.props.updateUserInfo(userData);
				toast.success(`Success`, {
					position: toast.POSITION.TOP_RIGHT
				  });
				document.cookie = `uToken=${token}`;
				setTimeout(() => {
					this.props.changeLoginInfo(true);
					// window.location.reload();
				}, 1000)
			} else {
				toast.error(`${json.message}`, {
					position: toast.POSITION.TOP_RIGHT
				  });
			}
			
			
			
		})
	}

	handleRegisterButtonClicked () {
		let email = this.state.registerEmail;
		let firstName = this.state.registerFirstName;
		let lastName = this.state.registerLastName;
		let password = this.state.registerPassword;



		this.client.register(firstName, lastName, email, password).then((json) => {
			if (json.status === "success") {
				toast.success(`user created. please login !!`, {
					position: toast.POSITION.TOP_RIGHT
				  });
				this.toggleLoginForm()
			} else {
				toast.error(`Some error occured !!`, {
					position: toast.POSITION.TOP_RIGHT
				});
			}
		})
	}


	render() {
		return (
			<div className="login-container" style={{ backgroundImage: `url(${bgImage})` }}>
				<LoginPage
					{...this.state}
					toggleLoginForm={this.toggleLoginForm}
					handleChangeInput={this.handleChangeInput}
					handleLoginButtonClicked={this.handleLoginButtonClicked}
					handleRegisterButtonClicked={this.handleRegisterButtonClicked}
				/>

				<ToastContainer />
			</div>
		)
	}
}


const mapStateToProps = state => {
	return {
		userData: state.updateUserInfo.isLoggedIn,
	};
  };
  
  const mapDispatchToProps = dispatch => {
	return {
		updateUserInfo: bindActionCreators(updateUserInfo, dispatch),
		changeLoginInfo: bindActionCreators(changeLoginInfo, dispatch),
	};
  };
  
  const connector = connect(mapStateToProps, mapDispatchToProps);
  
  
  export default connector(LoginContainer);