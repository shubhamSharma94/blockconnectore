import React from "react";


export default function LoginPage(props) {
	return (
		<div>
			{
				props.showLoginForm ? <LoginForm {...props}/> : <Register {...props}/>
			}
      
			
		</div>
	)
}


function LoginForm(props) {
	return (
		<div className="login-form">
			<input
				type="email"
				className="form-control margin-bottom-1rem"
				placeholder="Email"
				value={props.loginEmail ? props.loginEmail : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "loginEmail")
				}}
			/>

			<input
				type="password"
				className="form-control margin-bottom-1rem"
				placeholder="Password"
				value={props.loginPassword ? props.loginPassword : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "loginPassword")
				}}
			/>

			<button
				className="btn btn-default"
				onClick={() => {
					props.toggleLoginForm();
				}}
			>Not a User</button>
			<button
				className="btn btn-primary"
				disabled={props.loginEmail === "" || props.loginPassword === ""}
				onClick={() => {
					props.handleLoginButtonClicked()
				}}
			>SignIn</button>
		</div>
	)
}

function Register(props) {
	return (
		<div className="register-form">
			<input
				type="text"
				className="form-control margin-bottom-1rem"
				placeholder="First Name"
				value={props.registerFirstName ? props.registerFirstName : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "registerFirstName")
				}}
			/>

			<input
				type="text"
				className="form-control margin-bottom-1rem"
				placeholder="Last Name"
				value={props.registerLastName ? props.registerLastName : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "registerLastName")
				}}
			/>

			<input
				type="email"
				className="form-control margin-bottom-1rem"
				placeholder="Email"
				value={props.registerEmail ? props.registerEmail : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "registerEmail")
				}}
			/>

			<input
				type="password"
				className="form-control margin-bottom-1rem"
				placeholder="Password"
				value={props.registerPassword ? props.registerPassword : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "registerPassword")
				}}
			/>

			<input
				type="password"
				className="form-control margin-bottom-1rem"
				placeholder="Confirm Password"
				value={props.registerConfirmPassword ? props.registerConfirmPassword : ""}
				onChange={(e) => {
					props.handleChangeInput(e.target.value, "registerConfirmPassword")
				}}
			/>

			<button
				className="btn btn-default"
				onClick={() => {
					props.toggleLoginForm();

				}}
			>Already User</button>
			<button
				className="btn btn-primary"
				disabled={(props.registerPassword !== props.registerConfirmPassword) || (props.registerPassword === "" || props.registerConfirmPassword === "" || props.registerEmail === "") }
				onClick={() => {
					props.handleRegisterButtonClicked();
				}}
			>Register</button>
		</div>
	)
}
