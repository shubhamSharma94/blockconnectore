import React from "react";

class BootstrapModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	render() {
		return (
			<div className="modal fade in" tabIndex="-1" role="dialog">
				<div className="modal-dialog" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<button 
								type="button" 
								className="close" 
								onClick={() => {
									this.props.closeButtonAction();
								}}
							><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title">{this.props.title}</h4>
						</div>
						<div className="modal-body">
						{this.props.renderBody(this.state)}
						</div>
						<div className="modal-footer">
							<button 
								type="button" 
								className="btn btn-default" 
								onClick={() => {
									this.props.closeButtonAction();
								}}
							>{this.props.closeButtonText}</button>
							<button 
								type="button" 
								className="btn btn-primary"
								disabled={this.props.confirmButtonDisabled}
								onClick={() => {
									this.props.confirmButtonAction();
								}}
							>{this.props.confirmButtonText}</button>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default BootstrapModal;