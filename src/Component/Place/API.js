
export default function apiClient(token) {
    return {
  
      getPlaces: function() {
        let url = 'api/place';
        var paramObj = {
          token
        };

        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'GET',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },

      addPlace: function(name, description, address, imageData) {
        let url = 'api/place';
        var paramObj = {
          token,
          name,
          description,
          address,
          imageData
        };

        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'POST',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },


      updatePlace: function(placeId, name, description, address, imageData) {
        let url = 'api/place';
        var paramObj = {
          token,
          name,
          description,
          address,
          imageData,
          placeId
        };

        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'PUT',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },



      deletePlace: function(placeId) {
        let url = 'api/place';
        var paramObj = {
          token,
          placeId
        };

        var urlParameters = getAsUriParameters(paramObj);
        return fetch(url + '?' + urlParameters, {
          credentials: 'include',
          mode: 'cors',
          method: 'DELETE',
          // body: body,
        }).then((response) => {
          if (response.ok) {
            return response.json();
            
          }
          throw new Error("Error");
        });
      },
      



    };
  }

  function getAsUriParameters(data) {
     var url = '';
     for (var prop in data) {
        url += encodeURIComponent(prop) + '=' + 
            encodeURIComponent(data[prop]) + '&';
     }
     return url.substring(0, url.length - 1)
  }
