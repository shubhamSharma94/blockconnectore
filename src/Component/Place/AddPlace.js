import React, { Fragment } from "react";
import { Input, TextArea } from "../Common/Input";
import BootstrapModal from "../Modal/BootstrapModal";
import apiClient from "./API";
import { getCookie } from "../utils";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePlaceList } from '../../redux/actions/updatePlaceList';
import { changeAddPlaceModal } from '../../redux/actions/changeAddPlaceModal';
import { addPlace } from '../../redux/actions/addPlace';
import { updateFilterPlaceList } from '../../redux/actions/updateFilterPlaceList';
import { toast } from "react-toastify";

class AddPlace extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			description: "",
			address: "",
			image: "",
		}

		this.client = apiClient(getCookie('uToken'));
		this.handleInputChange = this.handleInputChange.bind(this);
		this.hideAddPlaceModal = this.hideAddPlaceModal.bind(this);
		this.handleAddPlace = this.handleAddPlace.bind(this);
		this.clearForm = this.clearForm.bind(this);
		this.handleUpdatePlace = this.handleUpdatePlace.bind(this);
		this.editPlaceList = this.editPlaceList.bind(this);
		this.setEditForm = this.setEditForm.bind(this);

	}

	componentWillMount() {
		
	}

	handleInputChange(value, type) {
		this.setState({
			[type]: value,
		})
	}

	hideAddPlaceModal () {
		this.props.changeAddPlaceModal(false);
		this.clearForm();
	}

	handleAddPlace() {
		let { name, description, address } = this.state;
		this.client.addPlace(name, description, address).then((json) => {
			this.props.addPlace(json.place);
			this.hideAddPlaceModal();
			this.clearForm();
			this.props.handlePlaceListUpdate();
			toast.success(`Success !! Place Added`, {
				position: toast.POSITION.TOP_RIGHT
			});
		})
	}

	clearForm () {
		this.setState({
			name: "",
			description: "",
			address: "",
			image: "",
		}, () => {
			if (this.props.editPlaceId !== "") {
				this.props.setEditId("")
			}
		})
	}

	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.editPlaceId !== "") {
			this.setEditForm(this.props.editPlaceId)
		}
	}

	setEditForm(placeId) {
		let placeList = this.props.placeList;
		placeList && placeList.forEach((elm) => {
			if (placeId === elm.id) {
				this.setState({
					name: elm.name,
					description: elm.description,
					address: elm.address,
					image: elm.imageUrl,
				})
			}
		})
	}

	handleUpdatePlace() {
		let { name, description, address } = this.state;
		let placeId = this.props.editPlaceId;
		this.client.updatePlace(placeId, name, description, address).then((json) => {
			if (json.status === "success") {
				this.editPlaceList();
			}
			this.hideAddPlaceModal();
			this.clearForm();
			this.props.handlePlaceListUpdate();
			toast.success(`Success !! Place Updated`, {
				position: toast.POSITION.TOP_RIGHT
			});
		})
	}

	editPlaceList () {
		let placeList = this.props.placeList;
		placeList && placeList.forEach((elm) => {
			elm.name = this.state.name;
			elm.description = this.state.description;
			elm.address = this.state.address;
			elm.imageUrl = this.state.imageUrl;
		})

		this.props.updatePlaceList(placeList);
		// this.props.updateFilterPlaceList(placeList);
		
	}

	render() {
		let { name, description, address } = this.state;
		return (
			this.props.showAddPlaceModal ? (
				<Fragment>
					<BootstrapModal
						title={`${this.props.editPlaceId === "" ? "Add" : "Update"} Place`}
						renderBody={() => {
							return (
								<AddPlaceForm
									{...this.state}
									handleInputChange={this.handleInputChange}
								/>
							)
						}}

						confirmButtonText={`${this.props.editPlaceId === "" ? "Add" : "Update"} Place`}
						confirmButtonDisabled={!name || !description || !address}
						confirmButtonAction={this.props.editPlaceId === "" ? this.handleAddPlace : this.handleUpdatePlace}

						closeButtonText="Close"
						closeButtonAction={this.hideAddPlaceModal}
					/>

				</Fragment>
			) : null

		)
	}
}




const mapStateToProps = state => {
	return {
		showAddPlaceModal: state.changeAddPlaceModal.showAddPlaceModal,
		placeList: state.updatePlaceList.placeList,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changeAddPlaceModal: bindActionCreators(changeAddPlaceModal, dispatch),
		addPlace: bindActionCreators(addPlace, dispatch),
		updateFilterPlaceList: bindActionCreators(updateFilterPlaceList, dispatch),
		updatePlaceList: bindActionCreators(updatePlaceList, dispatch),

		
	};
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(AddPlace);


function AddPlaceForm(props) {
	return (
		<Fragment>
			<Input
				type="text"
				placeholder="Enter Place Name"
				value={props.name}
				handleChange={props.handleInputChange}
				inputKey="name"
				className="margin-bottom-1rem"

			/>
			<TextArea
				placeholder="Enter Place Description"
				value={props.description}
				handleChange={props.handleInputChange}
				inputKey="description"
				className="margin-bottom-1rem"
			/>
			<TextArea
				placeholder="Enter Place Address"
				value={props.address}
				handleChange={props.handleInputChange}
				inputKey="address"
				className="margin-bottom-1rem"
			/>

			{/* <input type="file" onChange={(e) => {
				debugger
				props.handleInputChange(e.target.files[0], "image")
			}} /> */}
		</Fragment>
	)
}


