import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changePlaceCardExpand } from '../../redux/actions/changePlaceCardExpand';
import EditDeleteOption from "../Common/EditDeleteOption";

const defaultImage = "https://images.unsplash.com/photo-1562351768-f68650f3ec54?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80";

function PlaceCard(props) {
	return (
		<div
			className={`place-card ${props.placeData.id === props.expandedCardId ? "expand" : ""}`}
			onClick={() => {
				if (props.placeData.id !== props.expandedCardId)  {
					props.updatePlaceCardExpandId(props.placeData.id)

				}
			}}

		>
			{
				props.placeData.id === props.expandedCardId ? (
					<button
						type="button"
						className="close"
						onClick={() => {
							props.changePlaceCardExpand("");
						}}
					><span aria-hidden="true">&times;</span></button>
				) : null
			}

			<div className="pure-g full-height">
				<div className="pure-u-3-5 pure-u-md-4-5 full-height">
					<h3 className="name">{props.placeData.name}</h3>
					<p className="description">{props.placeData.description}</p>
					<p className="address">{props.placeData.address}</p>
				</div>
				<div className="pure-u-2-5 pure-u-md-1-5">
					<div className="place-image" style={{ backgroundImage: `url(${props.placeData.imageUrl ? props.placeData.imageUrl : defaultImage})` }}>
					</div>
				</div>
			</div>
			{
				props.placeData.id !== props.expandedCardId ? (
					<EditDeleteOption 
						handleDeleteClick={props.handlePlaceDelete}
						itemId={props.placeData.id}
						handleEditClick={props.setEditId}
					/>
				) : null
			}
			
		</div>
	)
}


const mapStateToProps = state => {
	return {
		expandedCardId: state.changePlaceCardExpand.expandedCardId,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changePlaceCardExpand: bindActionCreators(changePlaceCardExpand, dispatch),
	};
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(PlaceCard);