import React from "react";
import PlaceCard from "./PlaceCard.js";
import AddPlace from "./AddPlace.js";
import "../../css/place-list.css";
import apiClient from "./API";
import { getCookie } from "../utils";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePlaceList } from '../../redux/actions/updatePlaceList';
import { changeAddPlaceModal } from '../../redux/actions/changeAddPlaceModal';
import { changePlaceCardExpand } from '../../redux/actions/changePlaceCardExpand';
import { updateFilterPlaceList } from '../../redux/actions/updateFilterPlaceList';
import { ToastContainer, toast } from "react-toastify";

class PlaceListContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			searchString : "",
			filteredPlaceList: this.props.placeList,
			editPlaceId: "",
		}
		this.client = apiClient(getCookie('uToken'));
		this.getAllPlaceList = this.getAllPlaceList.bind(this);
		this.showAddPlaceModal = this.showAddPlaceModal.bind(this);
		this.updatePlaceCardExpandId = this.updatePlaceCardExpandId.bind(this);
		this.handleSearchInputChange = this.handleSearchInputChange.bind(this);
		this.handlePlaceListUpdate = this.handlePlaceListUpdate.bind(this);
		this.handlePlaceDelete = this.handlePlaceDelete.bind(this);
		this.setEditId = this.setEditId.bind(this);
	}


	getAllPlaceList() {
		this.client.getPlaces().then((json) => {
			this.props.updatePlaceList(json.place);
			this.props.updateFilterPlaceList(json.place);
		})
	}

	componentWillMount () {
		this.getAllPlaceList();
	}

	showAddPlaceModal() {
		this.props.changeAddPlaceModal(true)
	}

	updatePlaceCardExpandId(id) {
		this.props.changePlaceCardExpand(id);
	}

	handleSearchInputChange (value) {
		debugger
		let placeList = this.props.placeList;
		let searchString = value;

		let filteredPlaceList = filterByName(placeList, searchString);

		this.setState({
			searchString,
			filteredPlaceList
		}, () => {
			this.props.updateFilterPlaceList(filteredPlaceList)
		})

	}

	handlePlaceListUpdate () {
		let placeList = this.props.placeList;
		this.setState ( {
			searchString: "",
		}, () => {
			this.props.updateFilterPlaceList(placeList)
		})
	}

	handlePlaceDelete (placeId) {
		this.client.deletePlace(placeId).then((json) => {
			if (json.status === "success") {
				let placeList = this.props.placeList;
				let updatedPlaceList = [];
				placeList && placeList.forEach((elm, i) => {
					if (elm.id !== placeId ) {
						updatedPlaceList.push(elm);
					}
				})
				this.props.updateFilterPlaceList(updatedPlaceList)
				toast.success(`${json.status}`, {
					position: toast.POSITION.TOP_RIGHT
				});

			} else {
				toast.error(`${json.status}`, {
					position: toast.POSITION.TOP_RIGHT
				});
			}
		})
	}


	setEditId (placeId) {
		this.setState({
			editPlaceId: placeId,
		}, () => {
			if (placeId) {
				this.showAddPlaceModal();

			}
		})
	}

	render() {
		let filteredPlaceList = this.props.filteredPlaceList;

		return (
			<div className="place-list-container">
				<div className="text-left margin-top-1rem margin-bottom-1rem">
					<div className="pure-g">
						<div className="pure-u-1 ">
							<button
							className="btn btn-primary"
							onClick={() => {
								this.showAddPlaceModal()
							}}
						>
							Add Place
						</button>
						</div>
						<div className="pure-u-1 margin-top-1rem">
							<SearchBox 
								searchString={this.state.searchString}
								handleSearchInputChange={this.handleSearchInputChange}
							/>

						</div>
					</div>
					

				</div>

				{
					
					filteredPlaceList && filteredPlaceList.map((elm, i) => {
						return (
							<PlaceCard
								key={`place-${elm.id}`}
								placeData={elm}
								updatePlaceCardExpandId={this.updatePlaceCardExpandId}
								handlePlaceDelete={this.handlePlaceDelete}
								setEditId={this.setEditId}

							/>
						)
					})
				}

				<AddPlace 
					handlePlaceListUpdate={this.handlePlaceListUpdate}
					editPlaceId={this.state.editPlaceId}
					setEditId={this.setEditId}

				/>

				<ToastContainer />

			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		placeList: state.updatePlaceList.placeList,
		filteredPlaceList: state.updateFilterPlaceList.filteredPlaceList,
		
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updatePlaceList: bindActionCreators(updatePlaceList, dispatch),
		changeAddPlaceModal: bindActionCreators(changeAddPlaceModal, dispatch),
		changePlaceCardExpand: bindActionCreators(changePlaceCardExpand, dispatch),
		updateFilterPlaceList: bindActionCreators(updateFilterPlaceList, dispatch),
		
	};
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(PlaceListContainer);



function SearchBox	(props) {
	return (
		<div className="search-box">
			<input
				type="text"
				className="form-control"
				value={props.searchString}
				onChange={(e)=>{props.handleSearchInputChange(e.target.value)}}
			/>
			<span className="serach-icon glyphicon glyphicon-search"></span>
		</div>
	)
}


function filterByName (placeList, searchString) {
	debugger
	let filteredPlaceList = []

	placeList && placeList.forEach((place) => {
		if (place.name.toLowerCase().includes(searchString.toLowerCase())) {
			filteredPlaceList.push(place);
		}
	})

	return filteredPlaceList;
	
}