import React from "react";
import "../../css/topbar.css";
export default class Topbar extends React.Component{
    constructor (props) {
        super(props);
        this.state = {

        }

        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout () {
        document.cookie = `uToken=;expires=${Date.now()}` ;
        setTimeout (() => {
          window.open("/","_self")
        }, 500)
        
    }

    render () {
        return(
            <div className="topbar">
                <p>Places</p>
                <p 
                    className='logout'
                    onClick={() =>{
                        this.handleLogout();
                    }}
                >Logout</p>
            </div>
        )
    }
}