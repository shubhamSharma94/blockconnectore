import actionTypes from "../actionTypes/actionTypes";


export const addPlace = data => {
  return {
      type: actionTypes.ADD_PLACE,
      payload:  data ,
  };
};
