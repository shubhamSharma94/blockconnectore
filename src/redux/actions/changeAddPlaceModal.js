import actionTypes from "../actionTypes/actionTypes";


export const changeAddPlaceModal = data => {
  return {
      type: actionTypes.SHOW_ADD_PLACE_MODAL,
      payload:  data ,
  };
};
