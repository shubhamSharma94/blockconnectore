import actionTypes from "../actionTypes/actionTypes";



export const updateFilterPlaceList = data => {
  return {
      type: actionTypes.UPDATE_FILTER_PLACE_LIST,
      payload:  data ,
  };
};
