import actionTypes from "../actionTypes/actionTypes";



export const updatePlaceList = data => {
  return {
      type: actionTypes.UPDATE_PLACE_LIST,
      payload:  data ,
  };
};
