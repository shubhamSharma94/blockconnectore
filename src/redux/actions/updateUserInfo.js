import actionTypes from "../actionTypes/actionTypes";



export const updateUserInfo = data => {
  return {
      type: actionTypes.UPDATE_USER_INFO,
      payload:  data ,
  };
};
