import actionTypes from '../actionTypes/actionTypes';

let initialState = {
  showAddPlaceModal: false,
}

const changeAddPlaceModal = (state = initialState, action) => {
	let oldState = JSON.parse(JSON.stringify(state))
	switch (action.type) {

		case actionTypes.SHOW_ADD_PLACE_MODAL:
			return Object.assign({}, oldState, {
				showAddPlaceModal: action.payload
			})

		default:
			return state;
	}
};


export default changeAddPlaceModal;