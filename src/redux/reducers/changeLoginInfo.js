import actionTypes from '../actionTypes/actionTypes';

let initialState = {
  isLoggedIn: false,
}

const changeLoginInfo = (state = initialState, action) => {
	let oldState = JSON.parse(JSON.stringify(state))
	switch (action.type) {

		case actionTypes.CHANGE_LOGIN_DATA:
			return Object.assign({}, oldState, {
				isLoggedIn: action.payload
			})

		default:
			return state;
	}
};


export default changeLoginInfo;