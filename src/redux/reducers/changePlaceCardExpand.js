import actionTypes from '../actionTypes/actionTypes';

let initialState = {
  expandedCardId: "",
}

const changePlaceCardExpand = (state = initialState, action) => {
	let oldState = JSON.parse(JSON.stringify(state))
	switch (action.type) {

		case actionTypes.CHANGE_PLACE_CARD_EXPAND:
			return Object.assign({}, oldState, {
				expandedCardId: action.payload
			})

		default:
			return state;
	}
};


export default changePlaceCardExpand;