import { combineReducers } from 'redux';
import updateUserInfo from './updateUserInfo';
import changeLoginInfo from './changeLoginInfo';
import updatePlaceList from './updatePlaceList';
import changeAddPlaceModal from './changeAddPlaceModal';
import changePlaceCardExpand from './changePlaceCardExpand';
import updateFilterPlaceList from './updateFilterPlaceList';



const rootReducer = combineReducers({
    updateUserInfo,
    changeLoginInfo,
    updatePlaceList,
    changeAddPlaceModal,
    changePlaceCardExpand,
    updateFilterPlaceList
});

export default rootReducer;