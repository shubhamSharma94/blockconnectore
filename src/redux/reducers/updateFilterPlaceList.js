import actionTypes from '../actionTypes/actionTypes';

let initialState = {
	filteredPlaceList: [],
}

const updateFilterPlaceList = (state = initialState, action) => {
	let oldState = JSON.parse(JSON.stringify(state))
	switch (action.type) {

		case actionTypes.UPDATE_FILTER_PLACE_LIST:
			return Object.assign({}, oldState, {
				filteredPlaceList: action.payload
			})
		
		default:
			return state;
	}
};


export default updateFilterPlaceList;