import actionTypes from '../actionTypes/actionTypes';

let initialState = {
  placeList: [],
}

const updatePlaceList = (state = initialState, action) => {
	let oldState = JSON.parse(JSON.stringify(state))
	switch (action.type) {

		case actionTypes.UPDATE_PLACE_LIST:
			return Object.assign({}, oldState, {
				placeList: action.payload
			})
		
		case actionTypes.ADD_PLACE:
			return Object.assign({}, oldState, {
				placeList: oldState.placeList.concat(action.payload)
			})

		default:
			return state;
	}
};


export default updatePlaceList;