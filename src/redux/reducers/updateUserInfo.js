import actionTypes from '../actionTypes/actionTypes';

let initialState = {
  userData: {},
}

const updateUserData = (state = initialState, action) => {
	let oldState = JSON.parse(JSON.stringify(state))
	switch (action.type) {

		case actionTypes.UPDATE_USER_INFO:
			return Object.assign({}, oldState, {
				userData: action.payload
			})

		default:
			return state;
	}
};


export default updateUserData;